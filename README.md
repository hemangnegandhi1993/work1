# Data mining project Movie suggestion system
Data mining project for CSE 5334

Description: We live in a world where everybody has different taste of clothes, food and entertainment. 
A movie which is liked by a colleague might not be likes by me as I might have different taste than him. For answering this need I wish to create movie suggestion system which will help to give personalized inputs on the basis of individual choices and taste. This system will not only consider choices of the individual but will also consider the choices of people with the same taste of movies to provide personal suggestion of movies each user. Help for this project is received from classmate heet madhu. We will be having a database of few users in the start and as they grow based on the increasing amount of data the system will get better and better. 
In this project I have created an application with gives suggestion on the movies that user likes on the basis of his history of choices.
Database is from keggle ([DATABASE](https://www.kaggle.com/tmdb/tmdb-movie-metadata)).
Huge chunk of code written is from this GITHUB ACCOUNT [https://bit.ly/2GJaH8n](https://bit.ly/2GJaH8n) and google cloud console is used with help of VIDEO https://www.youtube.com/watch?v=RbejfDTHhhg

Development of project is in following phases:

Milestone 1:

We create the following:
*  Html file for getting user input (File = Input_page.html).
*  Python file for processing and inserting required data in pkl files (movie database from Keggle) (File = Preprocessing_work.py).
*  Web app with flask framework (File = Web_flask.py).
*  Html file for displaying answer (File = Result_page.html)


Milestone 2:
* html file for getting input(File = classification_input.html)
* Implement naivebayes in python(File = classification_file.py) .
* Implementing naivebayes to perform classification.
* html file for diplaying result(File = classification_result.html)

Milestone 3:
*  Creation of the suggestion system.







References:

* 	https://en.wikipedia.org/wiki/Naive_Bayes_classifier
*   https://www.kdnuggets.com/2016/02/nine-datasets-investigating-recommender-systems.html
*   http://shuaizhang.tech/2017/03/15/Datasets-For-Recommender-System/
* 	https://www.geeksforgeeks.org/flask-creating-first-simple-application/
* 	https://gist.github.com/entaroadun/1653794
* 	https://machinelearningmastery.com/naive-bayes-classifier-scratch-python/
* 	https://www.geeksforgeeks.org/removing-stop-words-nltk-python/
*   https://github.com/Heetmadhu
* 	https://docs.python.org/3/library/tokenize.html

*   https://www.geeksforgeeks.org/python-lemmatization-with-nltk/
*  	https://www.youtube.com/watch?v=RbejfDTHhhg
*  	https://gist.github.com/tuttelikz/94f750ef3bf14f8a126a
*  	https://github.com/gbroques/naive-bayes
*  	https://github.com/tillbe/Naive-Bayes-Python-Implementation
*  	https://github.com/topics/naive-bayes-algorithm?l=pythom
import pandas as pada
from sklearn.feature_extraction.text import CountVectorizer
from nltk.stem.snowball import SnowballStemmer
from nltk.corpus import stopwords
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.externals import joblib
from ast import literal_eval
import re
import numpy as np
keggle_credit = pada.read_csv("D:/zdata/Database1/Files/credits.csv") #credits
keggle_movie = pada.read_csv("D:/zdata/Database1/Files/movies.csv")   #movies

keggle = keggle_movie.merge(keggle_credit, left_on='MOVIE_ID', right_on='movie_id')
keggle_combines= pada.read_csv("D:/zdata/Database1/Work.csv")
keggle['tagline']=keggle['tagline'].fillna("")
keggle['original_title']=keggle['original_title'].fillna("")
keggle['cast']=keggle['cast'].fillna("")
keggle['crew']=keggle['crew'].fillna("")

keggle['genres']=keggle['genres'].fillna("")
keggle['cast_filtered'] = keggle['cast'].apply(literal_eval)
keggle['crew_filtered'] = keggle['crew'].apply(literal_eval)
keggle['cast'] = keggle['cast'].apply(literal_eval)
def get_name(casts):
    name_character = []
    for cast in casts:
        name_character.append(cast['name'])
        name_character.append(cast['character'])
    return name_character
keggle['cast_filtered'] = keggle['cast'].apply(get_name)

keggle['crew'] = keggle['crew'].apply(literal_eval)
category = ['Director', 'Writer', 'Editor']
def crew_names_important(data):
    crew_data = []
    for crew in data:
        if crew['job'] in category:
            crew_data.append(crew['name'])
    return crew_data
keggle['crew_filtered'] = keggle['crew'].apply(crew_names_important)


keggle['genres']=keggle['genres'].apply(literal_eval)
def genres_data(genres):
        genres_list = []
        for genre in genres:
                genres_list.append(genre['name'])
        return genres_list
keggle['genres_filtered'] = keggle['genres'].apply(genres_data)

def combine_data(data):
    return data['original_title']+' '+data['tagline']+'  '.join(data['cast_filtered'])+' '.join(data['crew_filtered'])+' '.join(data['genres_filtered'])
keggle['all_data']=keggle.apply(combine_data, axis = 1)

stop_words = stopwords.words('english')

def remove_stopwords(zz1):
    zz1 = re.sub('[^a-z\s]', '', zz1.lower())
    zz1 = [w for w in zz1.split() if w not in set(stop_words)]
    return ' '.join(zz1)

keggle['all_data']= keggle['all_data'].apply(remove_stopwords)
keggle['all_data'][0]

check1= SnowballStemmer('english')
analyzer = CountVectorizer().build_analyzer()
def stemming(data):
    return (check1.stem(w) for w in analyzer(data))
z1 = CountVectorizer(analyzer = stemming)
count_matrix = z1.fit_transform(keggle['all_data'])

z2 = TfidfTransformer()
z3= z2.fit_transform(count_matrix)

def get_search_results(query):
    d1 = remove_stopwords(query)
    d2 = z1.transform([query])
    d3 = z2.transform(d2)
    sim_score = cosine_similarity(d3, z3)
    numerical = np.argsort(sim_score).tolist()
    return keggle['original_title'].iloc[numerical[0][-7:]]

joblib.dump(z1, 'z1.pkl') #count
joblib.dump(z2, 'z2.pkl') #tdif transform
joblib.dump(z3, 'z3.pkl') #trained tfidf

data1= ['original_title','overview','tagline']# appending data of originaltitle into work_1 with pickle
keggle[data1].to_pickle('work_1.pkl') #movies
print("complete preprocessing work")
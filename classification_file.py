from flask import Flask, request, render_template
import pandas as pada
import numpy as np
import re
from nltk.corpus import stopwords
import nltk
import pickle
from nltk.stem import WordNetLemmatizer
from classification_file import claasification_function
nltk.download('stopwords')
from sklearn.externals import joblib
from sklearn.metrics.pairwise import cosine_similarity

from sklearn.feature_extraction.text import CountVectorizer
from nltk.stem.snowball import SnowballStemmer
keggle_credit = pada.read_csv("credits.csv")
keggle_movie = pada.read_csv("movies.csv")
keggle = keggle_movie.merge(keggle_credit, left_on='MOVIE_ID', right_on='movie_id')
keggle['tagline']=keggle['tagline'].fillna("")

keggle['original_title']=keggle['original_title'].fillna("")
keggle['cast']=keggle['cast'].fillna("")
keggle['crew']=keggle['crew'].fillna("")
keggle['genres']=keggle['genres'].fillna("")
english_stemmer = SnowballStemmer("english")
analyzer = CountVectorizer().build_analyzer()
app = Flask(__name__)
z1 = 'z1.pkl'
z2 = 'z2.pkl'
z3 = 'z3.pkl'

classification_file_work_1 = 'filework1.pkl'
classification_file_work_2 = 'filework2.pkl'
movies = 'movies.pkl'

stop_words = stopwords.words('english')
file_work1 = open(classification_file_work_1,'rb')
file_work2 = open(classification_file_work_2,'rb')

file_work1_train = pickle.load(file_work1)
file_work2_train = pickle.load(file_work2)
file_work2_train = file_work2[:,:4]
CATEGORY =  np.array(['Comedy','Action','Animation','Romance'])

classifier = claasification_function()
classifier.function_1(file_work1_train, file_work2_train, list(CATEGORY))

def process_test_classification(input_wrd):
    input_wrd = re.sub('[^a-z\s]', '', input_wrd.lower())
    input_wrd = [w for w in input_wrd.split() if w not in set(stop_words)]
    return ' '.join(input_wrd)

def stemming(input_wrd):
    return (english_stemmer.stem(w) for w in analyzer(input_wrd))

count_vector = joblib.load(z1)
tfidf_transformer = joblib.load(z2)
df_movies = pada.read_pickle(movies)
tfidf_trained = joblib.load(z3)

def result(input_wrd):
    input_wrd = process_input(input_wrd)
    pred = classifier.predict(input_wrd.split(' '))
    lst = []
    for i in range(CATEGORY.shape[0]):

        lst.append(pred[CATEGORY[i]])
    prediction_of_genre = CATEGORY[ids]
    arg = np.argsort(lst)
    arg = np.flip(arg,0)
    return {x:y for x,y in zip(prediction_of_genre[arg],lst[arg])}


@app.route('/result', methods=['POST'])
def result():
    try:
        input_wrd = request.form['input_wrd']
        input_wrd = process_input(input_wrd)
        z1 = count_vector.transform([input_wrd])
        z2 = tfidf_transformer.transform(z1)
        z3 = cosine_similarity(z2, tfidf_trained)
        z4 = np.argsort(z3).tolist()[0]
        result = z4[-10:]
        result_list = keggle.iloc[result]

    except Exception as e:
        print("Error:",e)
        return render_template('Input_page.html')
        print('else')
    return render_template('Result_page.html',list = result_list)

@app.route('/classification', methods=['POST'])
def classification():
    try:
        input_wrd = request.form['input_wrd']
        result_1 = predict(input_wrd)
    except Exception as e:
        print("Error:",e)
        return render_template('Classification_input.html')
    return render_template('Classification_result.html', text_area_value = input_wrd,result = result_1)

@app.route('/')
def index():
    return render_template('Input_page.html')

if __name__ == '__main__':
    app.run(host='127.0.0.1', port=8080, debug=True)